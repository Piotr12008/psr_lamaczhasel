﻿using CrackerClient.CrackerServiceRef;
using PasswordCracker;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace CrackerClientProgram
{
    public class ClientHandler : IPasswordCrackerCallback
    {
        public void AbortCracker()
        {
            Console.WriteLine("Przerywam");
        }

        public void RunBrute()
        {
            Console.WriteLine("Wykonuje bruta");
        }
        public void RunDictionary()
        {
            Console.WriteLine("Wykonuje dictionary");
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            InstanceContext instanceContext = new InstanceContext(new ClientHandler());
            CrackerClient client = new CrackerClient(instanceContext);


            client.Close();
        }
    }
}
