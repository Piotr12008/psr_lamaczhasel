﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Syndication;
using System.ServiceModel.Web;
using System.Text;

namespace PasswordCracker
{
    [ServiceContract(SessionMode = SessionMode.Required, CallbackContract = typeof(IPasswordCrackerCallback))]
    public interface ICracker
    {
        [OperationContract(IsOneWay = true)]
        void Metofa();

        [OperationContract]
        ClientData Join(string name);
        [OperationContract]
        void ReturnResult(ClientData clientData);

        //[OperationContract(IsOneWay = true)]
        //void SendResult(ClientData clientData);

    }
    public interface IPasswordCrackerCallback
    {
        [OperationContract(IsOneWay = true)]
        void AbortCracker();
        [OperationContract(IsOneWay = true)]
        void RunBrute(string password, int numFrom, int numTo, char[] characters, ClientData clientData);
        [OperationContract(IsOneWay = true)]
        void RunDictionary(string password, ClientData clientData, int packages, int grain);
        [OperationContract(IsOneWay = true)]
        void IsEnd();
        [OperationContract(IsOneWay = true)]
        void EndDictionary();
        [OperationContract(IsOneWay = true)]
        void SetDictionary(List<string> words);
        [OperationContract(IsOneWay = true)]
        void CheckDictionary(int n);
    }
    [DataContract]
    public class CompositeType
    {
        bool boolValue = true;
        string stringValue = "Hello ";

        [DataMember]
        public bool BoolValue
        {
            get { return boolValue; }
            set { boolValue = value; }
        }

        [DataMember]
        public string StringValue
        {
            get { return stringValue; }
            set { stringValue = value; }
        }
    }
}
