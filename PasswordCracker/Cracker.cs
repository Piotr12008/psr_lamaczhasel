﻿using System;
using System.Collections.Generic;
using System.IO;
using System.ServiceModel;
using System.Threading;
using static PasswordCracker.Enums;

namespace PasswordCracker
{

    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single, ConcurrencyMode = ConcurrencyMode.Multiple)]
    public class Cracker : ICracker
    {
        private static STAGE_TYPE stage;
        public static List<ClientData> ListOfClients = new List<ClientData>();
        public static List<string> messages = new List<string>();
        public static bool IsDone = false;
        public static string fileName = "slownik.txt";
        public static List<string> words = new List<string>();
        public static int packages;
        public static int packageNow;
        public static bool IsDictionary = false;
        public static string Password;
        public static int Grain;
        public static bool IsUpdate = false;
        IPasswordCrackerCallback callback;
        public Cracker()
        {
            stage = STAGE_TYPE.JOIN;
            CrackerHostInterface serviceCrackerInterface = CrackerHostInterface.GetInstance();
            serviceCrackerInterface.SingletonInstance = this;
        }
        public void Metofa()
        {

        }
        public void IsEnd()
        {
            for (int i = 0; i < ListOfClients.Count; i++)
            {
                try
                {
                    ListOfClients[i].Callback.IsEnd();
                }
                catch (Exception)
                {
                    break;
                }
            }
        }
        public void AbortCracker()
        {
            for (int i = 0; i < ListOfClients.Count; i++)
            {
                ListOfClients[i].Callback.AbortCracker();
            }
        }

        public void RunBrute(string password, int numFrom, int numTo, char[] characters)
        {
            char[] charactersZ = { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l'
                , 'm', 'n', 'o', 'p', 'r', 's', 't', 'u', 'w', 'q', 'y', 'z', 'x', 'v', '0', '1', '2', '3'
                , '4', '5', '6', '7', '8', '9' };

            int num = charactersZ.Length / ListOfClients.Count;
            for (int i = 0; i < ListOfClients.Count; i++)
            {

                ClientData clientData = ListOfClients[i];
                try
                {

                    if (i != 0)
                    {
                        numFrom = numFrom + num;
                    }
                    else
                    {
                        numFrom = 0;
                    }
                    if (i != ListOfClients.Count - 1)
                    {
                        numTo = numFrom + num;
                    }
                    else
                    {
                        numTo = charactersZ.Length - 1;
                    }
                    clientData.endTask = false;
                    clientData.password = "";
                    clientData.mess = "";
                    clientData.ExecuteTime = "";
                    clientData.operations = 0;
                    clientData.Callback.RunBrute(password, numFrom, numTo, charactersZ, clientData);

                    ListOfClients[i].mess = "Wykonuje bruta";


                }
                catch (TimeoutException toe)
                {
                    clientData.mess = "ERROR: Utracono połączenie z klientem.";
                    ListOfClients[i].mess = "ERROR: Utracono połączenie z klientem.";
                }
                catch (Exception cex)
                {
                    ListOfClients[i].mess = "ERROR: Błąd komunikacji";
                }
            }
        }
        public void DeleteClient(int id)
        {
            foreach (ClientData el in ListOfClients)
            {
                if (el.id == id)
                    ListOfClients.Remove(el);
            }
        }
        public bool SetDictionary()
        {
            try
            {
                using (StreamReader sr = File.OpenText(fileName))
                {
                    string s = "";
                    while ((s = sr.ReadLine()) != null)
                    {
                        words.Add(s);
                    }
                }
                if (words.Count == 0)
                    return false;
                else
                {
                    for (int i = 0; i < ListOfClients.Count; i++)
                    {
                        ListOfClients[i].Callback.SetDictionary(words);
                    }
                }
                return true;
            }
            catch (Exception e1)
            {
                for (int i = 0; i < ListOfClients.Count; i++)
                {
                    ListOfClients[i].mess = "ERROR: Brak pliku słownika";
                }
                return false;
            }
        }
        public void CheckDictionary()
        {
            for (int i = 0; i < ListOfClients.Count; i++)
            {
                ListOfClients[i].Callback.CheckDictionary(words.Count);
            }
        }
        public void RunDictionary(string password, ClientData clientData, int package, int grain)
        {
            Password = password;
            Grain = grain;
            packages = words.Count / grain;
            if (packages % grain > 0)
            {
                packages++;
            }
            for (int i = 0; i < ListOfClients.Count; i++)
            {
                package = packageNow - 1;
                packageNow++;
                if (packageNow > packages)
                {
                    ListOfClients[i].Callback.AbortCracker();
                    ListOfClients[i].mess = "WARNING: Skończyły się pozycje w słowniku, przerywam działanie";
                    packageNow = 0;
                    IsDictionary = false;
                    return;
                }
                ClientData cl = ListOfClients[i];
                try
                {
                    cl.endTask = false;
                    cl.password = "";
                    cl.mess = "";
                    cl.ExecuteTime = "";
                    cl.operations = 0;
                    IsDictionary = true;
                    cl.EndDictionary = true;
                    cl.Callback.RunDictionary(password, cl, package, grain);

                    ListOfClients[i].mess = "Wykonuje dictionary attack";
                }
                catch (TimeoutException toe)
                {
                    cl.mess = "ERROR: Utracono połączenie z klientem.";
                    ListOfClients[i].mess = "ERROR: Utracono połączenie z klientem.";
                }
                catch (Exception cex)
                {
                    ListOfClients[i].mess = "ERROR: Błąd komunikacji";
                }
            }
        }
        public void RunDictionaryOne(string password, ClientData clientData, int package, int grain)
        {
            Password = password;
            Grain = grain;
            packages = words.Count / grain;
            if (packages % grain > 0)
            {
                packages++;
            }
            for (int i = 0; i < ListOfClients.Count; i++)
            {
                if (ListOfClients[i].id == clientData.id)
                {
                    package = packageNow - 1;
                    packageNow++;
                    if (packageNow > packages)
                    {
                        ListOfClients[i].Callback.AbortCracker();
                        ListOfClients[i].mess = "WARNING: Skończyły się pozycje w słowniku, przerywam działanie";
                        packageNow = 0;
                        IsDictionary = false;
                        return;
                    }
                    ClientData cl = ListOfClients[i];
                    try
                    {
                        cl.endTask = false;
                        cl.password = "";
                        cl.mess = "";
                        cl.ExecuteTime = "";
                        clientData.operations = 0;
                        IsDictionary = true;
                        cl.EndDictionary = true;
                        cl.Callback.RunDictionary(password, cl, package, grain);

                        ListOfClients[i].mess = "Wykonuje dictionary attack";
                    }
                    catch (TimeoutException toe)
                    {
                        cl.mess = "ERROR: Utracono połączenie z klientem.";
                        ListOfClients[i].mess = "ERROR: Utracono połączenie z klientem.";
                    }
                    catch (Exception cex)
                    {
                        ListOfClients[i].mess = "ERROR: Błąd komunikacji";
                    }
                }
            }
        }
        public ClientData Join(string name)
        {
            if (stage == STAGE_TYPE.JOIN)
            {
                IPasswordCrackerCallback callback = OperationContext.Current.GetCallbackChannel<IPasswordCrackerCallback>();
                ClientData data = new ClientData(name, ListOfClients.Count, 2);
                data.Callback = callback;
                ListOfClients.Add(data);

                return data;
            }
            return null;
        }

        public void ReturnResult(ClientData clientData)
        {
            for (int i = 0; i < ListOfClients.Count; i++)
            {

                if (ListOfClients[i].id == clientData.id)
                {
                    if (clientData.UpdateDistionary)
                    {
                        IsUpdate = true;
                        ListOfClients[i].mess = clientData.mess;
                        ListOfClients[i].Callback.SetDictionary(words);
                        ListOfClients[i].UpdateDistionary = false;
                        clientData.UpdateDistionary = false;
                        return;
                    }
                    ListOfClients[i].mess = clientData.mess;
                    ListOfClients[i].operations = clientData.operations;
                    ListOfClients[i].password = clientData.password;
                    ListOfClients[i].endTask = clientData.endTask;
                    if (clientData.password != null && clientData.password != "")
                    {
                        AbortCracker();
                        IsDictionary = false;
                        packageNow = 0;
                        clientData.password = "";
                    }
                    else
                    {
                        if (IsDictionary)
                            RunDictionaryOne(Password, clientData, 0, Grain);
                        else
                        {
                            if (clientData.EndDictionary == false)
                                ListOfClients[i].Callback.EndDictionary();
                        }
                    }
                }
            }
        }

        public void OpenForClients()
        {
            ListOfClients.Clear();
            stage = STAGE_TYPE.JOIN;
        }

        IPasswordCrackerCallback Callback
        {
            get => callback;
            set => callback = value;
        }

    }
}
