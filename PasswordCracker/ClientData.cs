﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace PasswordCracker
{
    [DataContract]
    public class ClientData
    {
        [DataMember]
        public string name;
        [DataMember]
        public int id;
        [DataMember]
        public string password;
        [DataMember]
        public string mess;
        [DataMember]
        public bool endTask = false;
        [DataMember]
        public string ExecuteTime;
        [DataMember]
        public double operations;
        [DataMember]
        public bool EndDictionary = false;
        [DataMember]
        public bool UpdateDistionary = false;

        private IPasswordCrackerCallback callback;

        public ClientData() {}

        public ClientData(string name, int id, int numberOfThreads)
        {
            this.name = name;
            this.id = id;

        }
        public IPasswordCrackerCallback Callback { get => callback; set => callback = value; }
    }
}
