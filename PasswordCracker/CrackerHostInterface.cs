﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PasswordCracker
{
    public sealed class CrackerHostInterface
    {
        private static CrackerHostInterface instance = null;
        private static readonly object blocker = false;
        private bool isDataReady = false;
        private Cracker crackerInstance;
        private bool isCrackerInstanceReady = false;

        private CrackerHostInterface()
        {

        }
        public static CrackerHostInterface GetInstance()
        {
            if (instance == null)
            {
                instance = new CrackerHostInterface();
            }
            return instance;
        }
        public Cracker SingletonInstance
        {
            get
            {
                return crackerInstance;
            }
            set
            {
                isCrackerInstanceReady = true;
                crackerInstance = value;
            }
        }
        public bool IsDuplexInstanceReady
        {
            get
            {
                return isCrackerInstanceReady;
            }
        }

    }
}
