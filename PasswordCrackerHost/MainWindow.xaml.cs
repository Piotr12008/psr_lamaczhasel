﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Win32;
using PasswordCracker;
using MessageBox = System.Windows.MessageBox;

namespace PasswordCrackerHost
{
    /// <summary>
    /// Logika interakcji dla klasy MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public static ServiceHost selfHost;
        public static bool statusSerwerGV = false;
        public static bool statusCrackerGV = false;
        public static Cracker Cracker;
        public static List<int> StackChildrens = new List<int>();
        public static System.Windows.Threading.Dispatcher Dispatcher { get; set; }
        public static Thread Thread1;
        public static Stopwatch Sw = new Stopwatch();
        public static TimeSpan czasSuccess = new TimeSpan();
        public MainWindow()
        {
            InitializeComponent();
        }
        public void SetElements()
        {
            StackChildrens.Clear();
            foreach (TextBlock el in wynikiStack.Children)
            {
                StackChildrens.Add((int)el.Tag);
            }
        }
        public void SetNewClient()
        {
            TextBlock textBlock = new TextBlock();
            int id = 0;
            if (StackChildrens.Count != 0)
                id = StackChildrens.Last() + 1;
            textBlock.Tag = id;
            textBlock.Text = $"Klient #{id}: Oczekuję na zadanie...";
            wynikiStack.Children.Add(textBlock);
        }
        public void ClearChildren()
        {
            StackChildrens.Clear();
            wynikiStack.Children.Clear();
            Cracker.ListOfClients.Clear();
        }
        public void UpdateMessages(int id, string mess, double operations)
        {
            foreach (TextBlock el in wynikiStack.Children)
            {
                if ((int)el.Tag == id)
                {
                    el.Text = "Klient #" + id + ": " + mess;
                }
                if (mess != null && mess.Contains("ERROR:"))
                {
                    el.Foreground = Brushes.Red;
                    //Cracker.DeleteClient(id);
                }
                if (mess != null && mess.Contains("SUCCESS:"))
                {
                    //czasSuccess = time;
                    if (Sw.IsRunning)
                    {
                        Sw.Stop();
                        czasPelny.Foreground = Brushes.Green;
                        TimeSpan ts = Sw.Elapsed;
                        czasPelny.Text = String.Format("{0:00}:{1:00}:{2:00}.{3:00}", ts.Hours, ts.Minutes, ts.Seconds, ts.Milliseconds / 10);
                        czasKom.Foreground = Brushes.Green;
                        czasKom.Text =  (operations / ts.TotalMinutes).ToString("0.##");
                        Sw = new Stopwatch();
                    }
                }
            }

        }
        public void StatusUpdater()
        {
            while (true)
            {
                Action updateAction = new Action(() => { SetElements(); });
                Dispatcher.Invoke(updateAction);
                List<ClientData> clients = new List<ClientData>();
                clients.AddRange(Cracker.ListOfClients);
                if (StackChildrens.Count != clients.Count)
                {
                    Action updateAction2 = new Action(() => { SetNewClient(); });
                    Dispatcher.Invoke(updateAction2);
                }
                if (clients.Count != 0)
                {
                    bool isEnd = true;
                    foreach (ClientData el in clients)
                    {
                        if (el.mess != null && el.mess != "")
                        {
                            Action updateAction2 = new Action(() => { UpdateMessages(el.id, el.mess, el.operations); });
                            Dispatcher.Invoke(updateAction2);
                        }
                        isEnd = el.endTask;
                    }
                    if (isEnd)
                    {
                        Action updateAction2 = new Action(() => { ChangeInterfaceAbord("UKOŃCZONO", true); });
                        Dispatcher.Invoke(updateAction2);
                    }
                }
            }
        }
        public void StartUpdater()
        {
            Dispatcher = System.Windows.Threading.Dispatcher.CurrentDispatcher;
            Thread1 = new Thread(StatusUpdater);
            Thread1.Start();
        }
        public void ConnectButton_Click(object sender, RoutedEventArgs e)
        {
            runButton.IsEnabled = false;
            serverStatus.Text = "W TRAKCIE URUCHAMIANIA";
            serverStatus.Foreground = Brushes.Yellow;

            Uri baseAddress = new Uri("http://localhost:8001/CrackerService/");

            selfHost = new ServiceHost(typeof(Cracker), baseAddress);

            try
            {

                WSDualHttpBinding binding = new WSDualHttpBinding();

                binding.Security.Mode = WSDualHttpSecurityMode.None;
                binding.Security.Message.ClientCredentialType = MessageCredentialType.None;
                binding.OpenTimeout = new TimeSpan(0, 30, 0);

                ServiceThrottlingBehavior beh = new ServiceThrottlingBehavior();
                beh.MaxConcurrentSessions = 100;
                beh.MaxConcurrentCalls = 100;
                beh.MaxConcurrentInstances = 100;
                selfHost.Description.Behaviors.Add(beh);
                selfHost.AddServiceEndpoint(typeof(ICracker), binding, "CrackerService");

                ServiceMetadataBehavior smb = new ServiceMetadataBehavior();
                smb.HttpGetEnabled = true;

                selfHost.Description.Behaviors.Find<ServiceDebugBehavior>().IncludeExceptionDetailInFaults = true;
                selfHost.Description.Behaviors.Add(smb);

                selfHost.Open();
                serverStatus.Text = "URUCHOMIONY";
                serverStatus.Foreground = Brushes.Green;
                stopButton.IsEnabled = true;
                statusSerwerGV = true;

                CrackerHostInterface chi = CrackerHostInterface.GetInstance();

                Cracker = new Cracker();
                StartUpdater();
            }
            catch (Exception er)
            {
                selfHost.Abort();
                serverStatus.Text = "BŁĄD: ";
                serverStatus.Foreground = Brushes.Red;
                MessageBox.Show(er.Message + er.StackTrace);
                runButton.IsEnabled = true;
            }
        }

        private void DisconnectButton_Click(object sender, RoutedEventArgs e)
        {
            Cracker.IsEnd();
            Thread1.Abort();
            selfHost.Close();
            ClearChildren();
            serverStatus.Text = "ZATRZYMANY";
            serverStatus.Foreground = Brushes.Red;
            runButton.IsEnabled = true;
            stopButton.IsEnabled = false;
            statusSerwerGV = false;
            if (statusCrackerGV == true)
            {
                startButton.IsEnabled = true;
                abortButton.IsEnabled = false;
                crackerStatus.Text = "BŁĄD: SERWER WYŁĄCZONY";
                crackerStatus.Foreground = Brushes.Red;
            }
        }

        private void StartButton_Click(object sender, RoutedEventArgs e)
        {
            string result;
            if (statusSerwerGV == false)
            {
                MessageBox.Show("Serwer jest wyłączony");
                return;
            }
            if (hasloText.Text.Trim() == "")
            {
                MessageBox.Show("Nie podałeś hasła");
                return;
            }
            else if (hasloText.Text.Contains(" "))
            {
                MessageBox.Show("Hasło nie może zawierać spacji");
                return;
            }

            Regex regex = new Regex("[a-z0-9]");
            Match match = regex.Match(hasloText.Text);
            if (!match.Success)
            {
                MessageBox.Show("Hasło może zawierać tylko małe litery i cyfry, bez polskich znaków");
                return;
            }

            if (brute.IsChecked == true)
            {
                startButton.IsEnabled = false;
                abortButton.IsEnabled = true;
                crackerStatus.Text = "TRWA ŁAMANIE...";
                crackerStatus.Foreground = Brushes.Yellow;
                statusCrackerGV = true;

                if (!Sw.IsRunning)
                {
                    czasPelny.Foreground = Brushes.Yellow;
                    czasPelny.Text = "Trwa obliczanie...";
                    czasKom.Foreground = Brushes.Yellow;
                    czasKom.Text = "Trwa obliczanie...";
                    Sw.Start();
                }
                Cracker.RunBrute(hasloText.Text, 0, 0, null);
            }
            else if (dictionary.IsChecked == true)
            {
                startButton.IsEnabled = false;
                abortButton.IsEnabled = true;
                crackerStatus.Text = "TRWA ŁAMANIE...";
                crackerStatus.Foreground = Brushes.Yellow;
                statusCrackerGV = true;

                if (!Sw.IsRunning)
                {
                    czasPelny.Foreground = Brushes.Yellow;
                    czasPelny.Text = "Trwa obliczanie...";
                    czasKom.Foreground = Brushes.Yellow;
                    czasKom.Text = "Trwa obliczanie...";
                    Sw.Start();
                }
                if(Cracker.fileName == "")
                {
                    MessageBox.Show("Ustaw słownik");
                    return;
                }
                Regex reg = new Regex("[^0-9]");
                if (reg.IsMatch(packageSize.Text))
                {
                    MessageBox.Show("Dozwolone tylko liczby");
                    return;
                }
                Cracker.CheckDictionary();
                Thread.Sleep(1000);
                if (Cracker.IsUpdate)
                    return;
                Cracker.RunDictionary(hasloText.Text, null, 0, int.Parse(packageSize.Text));
            }
            else
            {
                MessageBox.Show("Zaznacz algorytm łamania hasła");
                return;
            }
        }

        private void Abort_Click(object sender, RoutedEventArgs e)
        {
            Cracker.AbortCracker();

            ChangeInterfaceAbord("PRZERWANE PRZEZ UŻYTK.", false);
        }
        public void ChangeInterfaceAbord(string desc, bool success)
        {            
            startButton.IsEnabled = true;
            abortButton.IsEnabled = false;
            crackerStatus.Text = desc;
            if (success == false)
                crackerStatus.Foreground = Brushes.Red;
            else
                crackerStatus.Foreground = Brushes.Green;
            statusCrackerGV = false;
        }

        private void BrowserButton_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog op = new OpenFileDialog();
            op.Title = "Wybierz baze";
            op.Filter = "All supported file|*.txt";
            if (op.ShowDialog() == true)
            {
                slownikText.Text = op.FileName;
            }
        }

        private void SetButton_Click(object sender, RoutedEventArgs e)
        {
            Cracker.fileName = slownikText.Text;
            Cracker.SetDictionary();
        }
        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            try
            {
                if (Cracker != null)
                    Cracker.IsEnd();
                if (Thread1.IsAlive)
                    Thread1.Abort();
                selfHost.Close();
            }
            catch (Exception) { }
        }
    }
}
