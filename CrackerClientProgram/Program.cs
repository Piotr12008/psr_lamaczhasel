﻿using CrackerClientProgram.CrackerServiceRef;
using PasswordCracker;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.ServiceModel;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CrackerClientProgram
{
    class Program
    {
        public static ManualResetEvent mre = new ManualResetEvent(false);
        public static ManualResetEvent mre2 = new ManualResetEvent(false);

        public class ClientHandler : ICrackerCallback
        {
            public static ClientData Data = null;
            public bool IsDone = false;
            public bool IsEndTask = false;
            public static bool EndProgram = false;
            public static Thread ThreadOperation;
            public static int dictCount { get; set; }
            public static List<string> dict { get; set; }
            public void AbortCracker()
            {
                if (ThreadOperation.IsAlive)
                {
                    ThreadOperation.Abort();
                    Console.WriteLine("WARNING: Przerwany przez serwer");
                    Data.mess = "WARNING: Przerwany przez serwer, nie znalazłem hasła";
                    Data.endTask = true;
                    IsEndTask = true;
                    mre2.Reset();
                    mre.Set();
                }
            }
            public void ExecuteBrute(string password, int numFrom, int numTo, char[] characters, ClientData clientData)
            {
                Data = clientData;
                Console.WriteLine("Wykonuje bruta");
                int passLength = password.Length;
                int[] indexes = new int[passLength];
                string pass = "";
                string hashpass = "";
                indexes[0] = numFrom;
                string foundPass = "";
                Console.WriteLine("Długość hasła: " + passLength);
                Stopwatch sw = new Stopwatch();
                sw.Start();

                using (MD5 hash = MD5.Create())
                {

                    switch (passLength)
                    {
                        case 1:
                            for (int i = numFrom; i < numTo; i++)
                            {
                                if (characters[i].ToString().Equals(password))
                                {
                                    foundPass = characters[i].ToString();
                                    break;
                                }
                            }
                            break;
                        case 2:
                            for (int i = numFrom; i < numTo; i++)
                            {
                                for (int j = 0; j < characters.Length; j++)
                                {
                                    pass = retCharacter(i, characters).ToString() + retCharacter(j, characters).ToString();
                                    hashpass = GetMd5Hash(pass);
                                    if (hashpass.Equals(password))
                                    {
                                        foundPass = pass;
                                        break;
                                    }
                                }
                                if (hashpass.Equals(password))
                                {
                                    foundPass = pass;
                                    break;
                                }
                            }
                            break;
                        case 3:
                            for (int i = numFrom; i < numTo; i++)
                            {
                                for (int j = 0; j < characters.Length; j++)
                                {
                                    for (int k = 0; k < characters.Length; k++)
                                    {
                                        pass = retCharacter(i, characters).ToString() + retCharacter(j, characters).ToString() + retCharacter(k, characters).ToString();
                                        hashpass = GetMd5Hash(pass);
                                        if (hashpass.Equals(password))
                                        {
                                            foundPass = pass;
                                            break;
                                        }
                                    }
                                    if (hashpass.Equals(password))
                                    {
                                        foundPass = pass;
                                        break;
                                    }
                                }
                                if (hashpass.Equals(password))
                                {
                                    foundPass = pass;
                                    break;
                                }
                            }
                            break;
                        case 4:
                            for (int i = numFrom; i < numTo; i++)
                            {
                                for (int j = 0; j < characters.Length; j++)
                                {
                                    for (int k = 0; k < characters.Length; k++)
                                    {
                                        for (int l = 0; l < characters.Length; l++)
                                        {
                                            pass = retCharacter(i, characters).ToString() + retCharacter(j, characters).ToString() + retCharacter(k, characters).ToString() + retCharacter(l, characters).ToString();
                                            hashpass = GetMd5Hash(pass);
                                            if (hashpass.Equals(password))
                                            {
                                                foundPass = pass;
                                                break;
                                            }
                                        }
                                        if (hashpass.Equals(password))
                                        {
                                            foundPass = pass;
                                            break;
                                        }
                                    }
                                    if (hashpass.Equals(password))
                                    {
                                        foundPass = pass;
                                        break;
                                    }
                                }
                                if (hashpass.Equals(password))
                                {
                                    foundPass = pass;
                                    break;
                                }
                            }
                            break;
                        case 5:
                            for (int i = numFrom; i < numTo; i++)
                            {
                                for (int j = 0; j < characters.Length; j++)
                                {
                                    for (int k = 0; k < characters.Length; k++)
                                    {
                                        for (int l = 0; l < characters.Length; l++)
                                        {
                                            for (int m = 0; m < characters.Length; m++)
                                            {
                                                pass = retCharacter(i, characters).ToString() + retCharacter(j, characters).ToString() + retCharacter(k, characters).ToString() + retCharacter(l, characters).ToString() + retCharacter(m, characters).ToString();
                                                hashpass = GetMd5Hash(pass);
                                                if (hashpass.Equals(password))
                                                {
                                                    foundPass = pass;
                                                    break;
                                                }
                                            }
                                            if (hashpass.Equals(password))
                                            {
                                                foundPass = pass;
                                                break;
                                            }
                                        }
                                        if (hashpass.Equals(password))
                                        {
                                            foundPass = pass;
                                            break;
                                        }
                                    }
                                    if (hashpass.Equals(password))
                                    {
                                        foundPass = pass;
                                        break;
                                    }
                                }
                                if (hashpass.Equals(password))
                                {
                                    foundPass = pass;
                                    break;
                                }
                            }
                            break;
                        case 6:
                            for (int i = numFrom; i < numTo; i++)
                            {
                                for (int j = 0; j < characters.Length; j++)
                                {
                                    for (int k = 0; k < characters.Length; k++)
                                    {
                                        for (int l = 0; l < characters.Length; l++)
                                        {
                                            for (int m = 0; m < characters.Length; m++)
                                            {
                                                for (int n = 0; n < characters.Length; n++)
                                                {
                                                    pass = retCharacter(i, characters).ToString() + retCharacter(j, characters).ToString() + retCharacter(k, characters).ToString() + retCharacter(l, characters).ToString() + retCharacter(m, characters).ToString() + retCharacter(n, characters).ToString();
                                                    hashpass = GetMd5Hash(pass);
                                                    if (hashpass.Equals(password))
                                                    {
                                                        foundPass = pass;
                                                        break;
                                                    }
                                                }
                                                if (hashpass.Equals(password))
                                                {
                                                    foundPass = pass;
                                                    break;
                                                }
                                            }
                                            if (hashpass.Equals(password))
                                            {
                                                foundPass = pass;
                                                break;
                                            }
                                        }
                                        if (hashpass.Equals(password))
                                        {
                                            foundPass = pass;
                                            break;
                                        }
                                    }
                                    if (hashpass.Equals(password))
                                    {
                                        foundPass = pass;
                                        break;
                                    }
                                }
                                if (hashpass.Equals(password))
                                {
                                    foundPass = pass;
                                    break;
                                }
                            }
                            break;
                        case 7:
                            for (int i = numFrom; i < numTo; i++)
                            {
                                for (int j = 0; j < characters.Length; j++)
                                {
                                    for (int k = 0; k < characters.Length; k++)
                                    {
                                        for (int l = 0; l < characters.Length; l++)
                                        {
                                            for (int m = 0; m < characters.Length; m++)
                                            {
                                                for (int n = 0; n < characters.Length; n++)
                                                {
                                                    for (int p = 0; p < characters.Length; p++)
                                                    {
                                                        pass = retCharacter(i, characters).ToString() + retCharacter(j, characters).ToString() + retCharacter(k, characters).ToString() + retCharacter(l, characters).ToString() + retCharacter(m, characters).ToString() + retCharacter(n, characters).ToString() + retCharacter(p, characters).ToString();
                                                        hashpass = GetMd5Hash(pass);
                                                        if (hashpass.Equals(password))
                                                        {
                                                            foundPass = pass;
                                                            break;
                                                        }
                                                    }
                                                    if (hashpass.Equals(password))
                                                    {
                                                        foundPass = pass;
                                                        break;
                                                    }
                                                }
                                                if (hashpass.Equals(password))
                                                {
                                                    foundPass = pass;
                                                    break;
                                                }
                                            }
                                            if (hashpass.Equals(password))
                                            {
                                                foundPass = pass;
                                                break;
                                            }
                                        }
                                        if (hashpass.Equals(password))
                                        {
                                            foundPass = pass;
                                            break;
                                        }
                                    }
                                    if (hashpass.Equals(password))
                                    {
                                        foundPass = pass;
                                        break;
                                    }
                                }
                                if (hashpass.Equals(password))
                                {
                                    foundPass = pass;
                                    break;
                                }
                            }
                            break;
                        default:
                            break;
                    }
                }
                sw.Stop();
                TimeSpan ts = sw.Elapsed;
                Data.ExecuteTime = String.Format("{0:00}:{1:00}:{2:00}.{3:00}", ts.Hours, ts.Minutes, ts.Seconds, ts.Milliseconds / 10);
                Data.operations = 0;
                if (!foundPass.Equals(""))
                {
                    Console.WriteLine("Hasło zostało złamane: -- " + foundPass + " -- Czas: " + Data.ExecuteTime);
                    Data.password = foundPass;
                    Data.mess = "SUCCESS: Client znalazł hasło: -- " + foundPass + " -- Czas: " + Data.ExecuteTime;
                }
                else
                {
                    Console.WriteLine("WARNING: Kończę działanie i nie znalazłem hasła. Czas: " + Data.ExecuteTime);
                    Data.mess = "WARNING: kończę działanie i nie znalazłem hasła. Czas: " + Data.ExecuteTime;
                }
                Data.endTask = true;
                IsEndTask = true;
                mre2.Reset();
                mre.Set();
            }
            public void RunBrute(string password, int numFrom, int numTo, char[] characters, ClientData clientData)
            {
                ThreadOperation = new Thread(() => { ExecuteBrute(password, numFrom, numTo, characters, clientData); });
                ThreadOperation.Start();
            }
            public void IsEnd()
            {
                EndProgram = true;
                Console.WriteLine("Serwer został zatrzymany");
                mre2.Reset();
                mre.Set();
            }
            public void ExecuteDictionary(string password, ClientData clientData, int package, int grain)
            {
                Console.WriteLine("Wykonuje dictionary-" + package);
                string foundPass = "";
                Stopwatch sw = new Stopwatch();
                sw.Start();
                List<string> words = new List<string>();
                words = dict.Skip(package * grain).Take(grain).ToList(); //System.IO.File.ReadLines("slownik" + clientData.id+ ".txt").Skip(package * grain).Take(grain).ToList();
                int i = 0;
                foreach (string el in words)
                {
                    string pass = GetMd5Hash(el);
                    if (pass.Equals(password))
                    {
                        foundPass = el;
                        i++;
                        break;
                    }
                }
                sw.Stop();
                TimeSpan ts = sw.Elapsed;
                Data.ExecuteTime = String.Format("{0:00}:{1:00}:{2:00}.{3:00}", ts.Hours, ts.Minutes, ts.Seconds, ts.Milliseconds / 10);
                Data.operations = package * grain + i;
                if (!foundPass.Equals(""))
                {
                    Console.WriteLine("Hasło zostało złamane: -- " + foundPass + " --");
                    Data.password = foundPass;
                    Data.mess = "SUCCESS: Client znalazł hasło: -- " + foundPass + " --" ;
                }
                else
                {
                    Data.password = foundPass;
                }
                mre2.Reset();
                mre.Set();
            }
            public void EndDictionary()
            {
                Console.WriteLine("WARNING: Kończę działanie i nie znalazłem hasła.");
                Data.mess = "WARNING: kończę działanie i nie znalazłem hasła.";
                Data.endTask = true;
                IsEndTask = true;
                mre2.Reset();
                mre.Set();
            }
            public void RunDictionary(string password, ClientData clientData, int package, int grain)
            {
                ThreadOperation = new Thread(() => { ExecuteDictionary(password, clientData, package, grain); });
                ThreadOperation.Start();
            }
            public void SetDictionary(List<string> words)
            {
                using (StreamWriter sw = File.CreateText("slownik" + ClientData.id + ".txt"))
                    foreach (string s in words)
                    {
                        sw.WriteLine(s);
                    }

                dictCount = words.Count();
                Console.WriteLine("Zaktualizowałem słownik");
                Data.mess = "SUCCESS: Zaktualizowałem słownik";
                mre2.Reset();
                mre.Set();
            }
            public void CheckDictionary(int n)
            {
                if (dictCount != n)
                {
                    Data.mess = "WARNING: Aktualizacja słownika";
                    Data.UpdateDistionary = true;
                    mre2.Reset();
                    mre.Set();
                }
            }
            public void SendingDone()
            {
                mre.Reset();
                mre2.Set();
                IsDone = true;
            }
            public char retCharacter(int num, char[] characters)
            {
                return characters[num];
            }
            public ClientData ClientData
            {
                get
                {
                    return Data;
                }
                set
                {
                    Data = value;
                }
            }
            public int DictCount
            {
                get
                {
                    return dictCount;
                }
                set
                {
                    dictCount = value;
                }
            }
            public List<string> _dict
            {
                get
                {
                    return dict;
                }
                set
                {
                    dict = value;
                }
            }
            public bool IsEndProgram
            {
                get
                {
                    return EndProgram;
                }
                set
                {
                    EndProgram = value;
                }
            }
        }

        public static string GetMd5Hash(string input)
        {
            string key = "psrJestSuper11hj";
            using (var md5 = new MD5CryptoServiceProvider())
            {
                using (var tdes = new TripleDESCryptoServiceProvider())
                {
                    tdes.Key = md5.ComputeHash(UTF8Encoding.UTF8.GetBytes(key));
                    tdes.Mode = CipherMode.ECB;
                    tdes.Padding = PaddingMode.PKCS7;

                    using (var transform = tdes.CreateEncryptor())
                    {
                        byte[] textBytes = UTF8Encoding.UTF8.GetBytes(input);
                        byte[] bytes = transform.TransformFinalBlock(textBytes, 0, textBytes.Length);
                        return Convert.ToBase64String(bytes, 0, bytes.Length);
                    }
                }
            }
        }
        static void Main(string[] args)
        {
            ClientHandler callbackHandler = new ClientHandler();
            InstanceContext instanceContext = new InstanceContext(new ClientHandler());
            CrackerClient client = new CrackerClient(instanceContext);
            try
            {
                Console.Write("Podaj ip hosta: ");
                string input;
                input = Console.ReadLine();
                //Console.WriteLine();

                Uri uri = new Uri("http://" + input + ":8001/CrackerService/CrackerService");
                client.Endpoint.Address = new EndpointAddress(uri);
                Console.WriteLine("URI: " + client.Endpoint.Address);

                Console.WriteLine("Trwa nawiązywanie połączenia z hostem.");
                client.Open();

                Console.WriteLine("Aplikacja klienta działa.");

                ClientData data = client.Join(Environment.MachineName);
                if (data != null)
                {
                    Console.WriteLine("Otrzymałem id: {0} jako {1}", data.id, data.name);
                    Thread.Sleep(1000);
                }
                else Console.WriteLine("Błąd.");

                try
                {
                    callbackHandler.DictCount = File.ReadLines("slownik" + data.id + ".txt").Count();
                    callbackHandler._dict = File.ReadAllLines("slownik" + data.id + ".txt").ToList();
                }
                catch (Exception e)
                {
                    Console.WriteLine("Nie mam pliku słownika");
                }

                callbackHandler.ClientData = data;
                Console.WriteLine("Oczekiwanie na zadanie.");

                do
                {
                    mre.WaitOne();

                    //Thread.Sleep(50);
                    if (callbackHandler.IsEndProgram)
                        break;
                    client.ReturnResult(callbackHandler.ClientData);
                    callbackHandler.SendingDone();

                    mre2.WaitOne();

                } while (!callbackHandler.IsEndProgram);

                Console.WriteLine("Wciśnij przycisk aby zamknąć klienta");

                client.Close();

                Console.ReadKey();
            }
            catch (Exception e1)
            {
                Console.WriteLine(e1.Message);
            }


        }
    }
}
